package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {

    private int id;
    private double price;
    private Set<Cart> carts = new HashSet<>();

    public Item(double total) {
        this.price = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return id == item.id &&
                Double.compare(item.price, price) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price);
    }
}
