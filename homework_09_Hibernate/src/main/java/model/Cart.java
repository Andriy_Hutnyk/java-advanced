package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart {

    private int id;
    private String name;
    private double total;
    private Set<Item> items = new HashSet<>();

    public Cart(double total, String name) {
        this.total = total;
        this.name = name;
    }

    public Cart(String name) {
        this.name = name;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
        this.total = getTotal(items);
    }

    private double getTotal(Set<Item> items) {
        return items.stream().mapToDouble(Item::getPrice).sum();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cart cart = (Cart) o;
        return id == cart.id &&
                Double.compare(cart.total, total) == 0 &&
                Objects.equals(name, cart.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, total);
    }
}
