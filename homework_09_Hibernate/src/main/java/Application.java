import model.Cart;
import model.Item;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.SessionFactoryUtil;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Application {

    public static void main(String[] args) {

        Session session = SessionFactoryUtil.getSession();

        Transaction transaction = session.beginTransaction();

        Cart cart1 = new Cart("Cart-1");

        Item item1 = new Item(100);
        Item item2 = new Item(200);
        Item item3 = new Item(300);

        Set<Item> set1 = new HashSet<>(Arrays.asList(item1, item2, item3));
        cart1.setItems(set1);

        session.save(cart1);
        transaction.commit();
        session.close();

    }
}
