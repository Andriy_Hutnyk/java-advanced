package service.impl;

import dao.Blog;
import exception.BlogAlreadyExistsException;
import exception.NoSuchBlogException;
import jdbc.MySqlConnector;
import org.apache.log4j.Logger;
import service.BlogService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BlogServiceImpl implements BlogService {

    private static final Logger logger = Logger.getLogger(BlogServiceImpl.class);
    private static Connection connection;

    static {
        try {
            connection = MySqlConnector.getConnection();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Blog> getAllBlogs() {
        List<Blog> blogs = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM web.blogs")) {
            while (resultSet.next()) {
                Blog blog = new Blog(resultSet.getInt("id"), resultSet.getString("name"));
                blogs.add(blog);
            }
            logger.info("Get all blogs request was processed");
        } catch (SQLException e) {
            logger.error(e.getStackTrace());
        }
        return blogs;
    }

    @Override
    public Blog getBlogById(int id) throws SQLException {
        Blog blog = null;
        ResultSet resultSet = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM web.blogs WHERE id = ?")) {
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                blog = new Blog(resultSet.getInt("id"), resultSet.getString("name"));
                logger.info("Getting blog by id: " + id);
            } else {
                logger.error("No blog with id: " + id);
                throw new NoSuchBlogException("No blog with id: " + id);
            }
        } catch (SQLException | NoSuchBlogException e) {
            logger.error(e.getStackTrace());
        } finally {
            resultSet.close();
        }
        return blog;
    }

    @Override
    public void createBlog(Blog blog) throws BlogAlreadyExistsException, SQLException {
        if (isExist(blog.getId())) {
            logger.error("Blog with id " + blog.getId() + "already exists");
            throw new BlogAlreadyExistsException("Blog with id \" + blog.getId() + \"already exists");
        } else {
            try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO web.blogs (id, name) VALUES (?, ?)")) {
                preparedStatement.setInt(1, blog.getId());
                preparedStatement.setString(2, blog.getName());
                preparedStatement.execute();
                logger.info("Creating new blog " + blog);
            } catch (SQLException e) {
                logger.error(e.getStackTrace());
            }
        }
    }

    @Override
    public void updateBlog(int id, Blog blog) throws NoSuchBlogException, SQLException {
        if (isExist(id)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE web.blogs SET id = ?, name = ? WHERE (id = ?)")) {
                preparedStatement.setInt(1, blog.getId());
                preparedStatement.setString(2, blog.getName());
                preparedStatement.setInt(3, id);
                preparedStatement.execute();
                logger.info("Updating blog with id: " + id);
            } catch (SQLException e) {
                logger.error(e.getStackTrace());
            }
        } else {
            logger.error("No blog with id: " + id);
            throw new NoSuchBlogException("No blog with id: " + id);
        }
    }

    @Override
    public void deleteBlog(int id) throws NoSuchBlogException, SQLException {
        if (isExist(id)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM web.blogs WHERE id = ?")) {
                preparedStatement.setInt(1, id);
                preparedStatement.execute();
            } catch (SQLException e) {
                logger.error(e.getStackTrace());
            }
        }else {
            logger.error("No blog with id: " + id);
            throw new NoSuchBlogException("No blog with id: " + id);
        }
    }

    @Override
    public boolean isExist(int blogId) throws SQLException {
        boolean flag = false;
        for (Blog blog : getAllBlogs()) {
            flag = blog.getId() == blogId;
            if (flag) {
                logger.info("Blog with id:" + blogId + "is exist");
                break;
            }
        }
        return flag;
    }
}
