package service.impl;

import dao.Article;
import dao.Blog;
import exception.ArticleAlreadyExistsException;
import exception.NoSuchArticleException;
import exception.NoSuchBlogException;
import jdbc.MySqlConnector;
import org.apache.log4j.Logger;
import service.ArticleService;
import service.BlogService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArticleServiceImpl implements ArticleService {

    private static final Logger logger = Logger.getLogger(ArticleServiceImpl.class);
    private static BlogService blogService = new BlogServiceImpl();
    private static Connection connection;

    static {
        try {
            connection = MySqlConnector.getConnection();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Article> getAllArticles() {
        List<Article> articles = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM web.articles")) {
            while (resultSet.next()) {
                Blog blog = blogService.getBlogById(resultSet.getInt("blog_id"));
                Article article = new Article(resultSet.getInt("article_id"), resultSet.getString("title"),
                        resultSet.getString("text"), blog);
                articles.add(article);
            }
            logger.info("Get all articles request was processed");
        } catch (SQLException e) {
            logger.error(e.getStackTrace());
        }
        return articles;
    }

    @Override
    public Article getArticleById(int id) throws SQLException {
        Article article = null;
        ResultSet resultSet = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM web.articles WHERE article_id = ?")) {
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Blog blog = blogService.getBlogById(resultSet.getInt("blog_id"));
                article = new Article(resultSet.getInt("article_id"), resultSet.getString("title"),
                        resultSet.getString("text"), blog);
            } else {
                logger.error("No article with id : " + id);
                throw new NoSuchArticleException("No article with id : " + id);
            }
        } catch (SQLException | NoSuchArticleException e) {
            logger.error(e.getStackTrace());
        } finally {
            resultSet.close();
        }

        return article;
    }

    @Override
    public void createArticle(Article article) throws NoSuchBlogException, ArticleAlreadyExistsException, SQLException {
        if (blogService.isExist(article.getBlog().getId())) {
            if (isExist(article.getArticleId())) {
                logger.error("Article " + article + "is already exists");
                throw new ArticleAlreadyExistsException("Article " + article + "is already exists");
            } else {
                try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO web.articles " +
                        "(article_id, title, text, blog_id) VALUES (?, ?, ?, ?)")) {
                    preparedStatement.setInt(1, article.getArticleId());
                    preparedStatement.setString(2, article.getTitle());
                    preparedStatement.setString(3, article.getText());
                    preparedStatement.setInt(4, article.getBlog().getId());
                    preparedStatement.execute();
                    logger.info("Creating new article " + article);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        } else {
            logger.error("Blog" + article.getBlog() + "is not exists");
            throw new NoSuchBlogException("Blog" + article.getBlog() + "is not exists");
        }
    }

    @Override
    public void updateArticle(int id, Article article) throws NoSuchBlogException, SQLException, NoSuchArticleException {
        if (isExist(id)) {
            if (blogService.isExist(article.getBlog().getId())) {
                try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE web.articles SET article_id = ?, " +
                        "title = ?, text = ?, blog_id = ? WHERE (article_id = ?)")) {
                    preparedStatement.setInt(1, article.getArticleId());
                    preparedStatement.setString(2, article.getTitle());
                    preparedStatement.setString(3, article.getText());
                    preparedStatement.setInt(4, article.getBlog().getId());
                    preparedStatement.setInt(5, id);
                    preparedStatement.execute();
                    logger.info("Updating article with id: " + id);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                logger.error("No such blog :" + article.getBlog());
                throw new NoSuchBlogException("No such blog :" + article.getBlog());
            }
        } else {
            logger.error("No article with id: " + id);
            throw new NoSuchArticleException("No article with id: " + id);
        }

    }

    @Override
    public void deleteArticle(int id) throws NoSuchArticleException, SQLException {
        if (isExist(id)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM web.articles WHERE article_id = ?")) {
                preparedStatement.setInt(1, id);
                preparedStatement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else {
            logger.error("No article with id" + id);
            throw new NoSuchArticleException("No article with id" + id);
        }
    }

    @Override
    public boolean isExist(int id) throws SQLException {
        boolean flag = false;
        for (Article article : getAllArticles()) {
            flag = article.getArticleId() == id;
            if (flag) {
                logger.info("Article with id:" + id + "is exist");
                break;
            }
        }
        return false;
    }
}
