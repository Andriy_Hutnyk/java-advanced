package service;

import dao.Blog;
import exception.BlogAlreadyExistsException;
import exception.NoSuchBlogException;

import java.sql.SQLException;
import java.util.List;

public interface BlogService {

    List<Blog> getAllBlogs() throws SQLException;

    Blog getBlogById(int id) throws SQLException;

    void createBlog(Blog blog) throws SQLException, BlogAlreadyExistsException;

    void updateBlog(int id, Blog blog) throws NoSuchBlogException, SQLException;

    void deleteBlog(int id) throws SQLException, NoSuchBlogException;

    boolean isExist (int id) throws SQLException;
}
