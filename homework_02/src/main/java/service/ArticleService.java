package service;

import dao.Article;
import exception.ArticleAlreadyExistsException;
import exception.NoSuchArticleException;
import exception.NoSuchBlogException;

import java.sql.SQLException;
import java.util.List;

public interface ArticleService {

    List<Article> getAllArticles() throws SQLException;

    Article getArticleById(int id) throws SQLException;

    void createArticle(Article article) throws SQLException, NoSuchBlogException, ArticleAlreadyExistsException;

    void updateArticle(int id, Article article) throws SQLException, NoSuchBlogException, NoSuchArticleException;

    void deleteArticle(int id) throws SQLException, NoSuchArticleException;

    boolean isExist (int id) throws SQLException;

}
