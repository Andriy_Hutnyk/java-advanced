package jdbc;

import org.apache.log4j.Logger;
import service.ArticleService;
import service.impl.ArticleServiceImpl;
import service.BlogService;
import service.impl.BlogServiceImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlConnector {

    private static final Logger logger = Logger.getLogger(MySqlConnector.class);
    private static final String MYSQL_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost/web?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    private static final String USER = "root";
    private static final String PASSWORD = "50arorep";

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName(MYSQL_DRIVER);
        logger.info("Connection is open");
        return DriverManager.getConnection(URL, USER, PASSWORD);
    }

}
