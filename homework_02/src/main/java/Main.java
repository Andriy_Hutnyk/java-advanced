import service.ArticleService;
import service.BlogService;
import service.impl.ArticleServiceImpl;
import service.impl.BlogServiceImpl;

import java.sql.SQLException;

public class Main {

    private static BlogService blogService = new BlogServiceImpl();
    private static ArticleService articleService = new ArticleServiceImpl();

    public static void main(String[] args) throws SQLException {
        //System.out.println(blogService.getAllBlogs());
        System.out.println(blogService.getBlogById(7));
        //blogService.deleteBlog(3);
        //blogService.createBlog(new Blog(3, "Basketball"));
        //blogService.updateBlog(3, new Blog(3, "Football"));

        //System.out.println(articleService.getAllArticles());
        //System.out.println(articleService.getArticleById(2));
        //articleService.updateArticle(4, new Article(5, "test", "t", new Blog(1, "Hokey")));
        //articleService.createArticle(new Article(4, "rrr", "rrrrrrrrrrr", new Blog(1, "Hokey")));
        //articleService.deleteArticle(4);
    }
}
