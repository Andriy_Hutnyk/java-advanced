package exception;

public class ArticleAlreadyExistsException extends Exception {
    public ArticleAlreadyExistsException(String message) {
        super(message);
    }
}
