package exception;

public class NoSuchArticleException extends Exception {
    public NoSuchArticleException(String message) {
        super(message);
    }
}
