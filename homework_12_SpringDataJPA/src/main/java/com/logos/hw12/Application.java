package com.logos.hw12;

import com.logos.hw12.model.EducationalInstitution;
import com.logos.hw12.service.EducationalInstitutionService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
		EducationalInstitutionService service = context.getBean(EducationalInstitutionService.class);

		EducationalInstitution eduIns1 = new EducationalInstitution(1, "UAD", 4, 8,
				1800, "Lviv, Pidgolosko 27");
		EducationalInstitution eduIns2 = new EducationalInstitution(2, "Lvivska Politehnika", 5, 16,
				3400, "Lviv, Stepana Banderu 8");
		EducationalInstitution eduIns3 = new EducationalInstitution(3, "Kuivska Politehnika", 5, 18,
				6100, "Kyiv, Shevchenka 11");
		EducationalInstitution eduIns4 = new EducationalInstitution(4, "Rivne Pidvodnik", 3, 5,
				900, "Rivne, Lesi Ukrainky 5");
		EducationalInstitution eduIns5 = new EducationalInstitution(6, "For deletion", 11, 11,
				11, "For deletion");

		//Create
		/*service.save(eduIns1);
		service.save(eduIns2);
		service.save(eduIns3);
		service.save(eduIns4);*/
		service.save(eduIns5);

		//Read
		System.out.println("Read all");
		service.findAll().forEach(System.out::println);

		System.out.println("Read by id");
		System.out.println(service.findById(1));

		System.out.println("Read by level");
		service.findByLevelOrderById(5).forEach(System.out::println);

		System.out.println("Read by number of students and number of institutions");
		service.findByNumberOfStudentsGreaterThanAndNumberOfInstitutionsLessThan(1000, 12).
				forEach(System.out::println);

		System.out.println("Read by address");
		service.findByAddressContaining("Lviv").forEach(System.out::println);

		System.out.println("Read by students between");
		service.findByNumberOfStudentsBetween(1000, 2000).forEach(System.out::println);

		//Update
		EducationalInstitution institution = service.findById(4);
		institution.setAddress("Rivne, Lesi Ukrainky 5 - Updated");
		institution.setName("Rivne Pidvodnik - Updated");
		service.update(institution);

		//Delete
		service.deleteById(6);

	}

}
