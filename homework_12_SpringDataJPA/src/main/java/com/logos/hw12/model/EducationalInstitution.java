package com.logos.hw12.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "educational_institution")
public class EducationalInstitution {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "level")
    private int level;

    @Column(name = "number_of_institutions")
    private int numberOfInstitutions;

    @Column(name = "number_of_students")
    private int numberOfStudents;

    @Column(name = "address")
    private String address;

}
