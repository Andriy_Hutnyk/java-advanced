package com.logos.hw12.repository;

import com.logos.hw12.model.EducationalInstitution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EducationalInstitutionRepository extends JpaRepository<EducationalInstitution, Integer>, CrudRepository<EducationalInstitution, Integer> {

    List<EducationalInstitution> findByLevelOrderById(int level);

    List<EducationalInstitution> findByNumberOfStudentsGreaterThanAndNumberOfInstitutionsLessThan(int numOfStud, int numOfInst);

    List<EducationalInstitution> findByAddressContaining(String town);

    List<EducationalInstitution> findByNumberOfStudentsBetween(int a, int b);
}
