package com.logos.hw12.service;

import com.logos.hw12.model.EducationalInstitution;

import java.util.List;

public interface EducationalInstitutionService {

    EducationalInstitution save(EducationalInstitution educationalInstitution);

    EducationalInstitution findById(int id);

    List<EducationalInstitution> findAll();

    EducationalInstitution update(EducationalInstitution educationalInstitution);

    void deleteById(int id);

    List<EducationalInstitution> findByLevelOrderById(int level);

    List<EducationalInstitution> findByNumberOfStudentsGreaterThanAndNumberOfInstitutionsLessThan(int numOfStud, int numOfInst);

    List<EducationalInstitution> findByAddressContaining(String town);

    List<EducationalInstitution> findByNumberOfStudentsBetween(int a, int b);


}
