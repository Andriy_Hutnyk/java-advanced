package com.logos.hw12.service.impl;

import com.logos.hw12.model.EducationalInstitution;
import com.logos.hw12.repository.EducationalInstitutionRepository;
import com.logos.hw12.service.EducationalInstitutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EducationalInstitutionServiceImpl implements EducationalInstitutionService {

    @Autowired
    private EducationalInstitutionRepository repository;

    @Override
    public EducationalInstitution save(EducationalInstitution educationalInstitution) {
        return repository.save(educationalInstitution);
    }

    @Override
    public EducationalInstitution findById(int id) {
        return repository.findById(id).orElseThrow(NullPointerException::new);
    }

    @Override
    public List<EducationalInstitution> findAll() {
        return repository.findAll();
    }

    @Override
    public EducationalInstitution update(EducationalInstitution educationalInstitution) {
        return repository.save(educationalInstitution);
    }

    @Override
    public void deleteById(int id) {
        repository.deleteById(id);
    }

    @Override
    public List<EducationalInstitution> findByLevelOrderById(int level) {
        return repository.findByLevelOrderById(level);
    }

    @Override
    public List<EducationalInstitution> findByNumberOfStudentsGreaterThanAndNumberOfInstitutionsLessThan(int numOfStud, int numOfInst) {
        return repository.findByNumberOfStudentsGreaterThanAndNumberOfInstitutionsLessThan(numOfStud, numOfInst);
    }

    @Override
    public List<EducationalInstitution> findByAddressContaining(String town) {
        return repository.findByAddressContaining(town);
    }

    @Override
    public List<EducationalInstitution> findByNumberOfStudentsBetween(int a, int b) {
        return repository.findByNumberOfStudentsBetween(a, b);
    }
}
