package service;

import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Project;
import model.TeamLead;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import service.impl.ProjectServiceImpl;
import service.impl.TeamLeadServiceImpl;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class TeamLeadServiceImplTest {

    private static TeamLeadServiceImpl teamLeadService;
    private static ProjectServiceImpl projectService;

    @BeforeAll
    static void init() {
        teamLeadService = new TeamLeadServiceImpl();
        projectService = new ProjectServiceImpl();
    }

    @Test
    @DisplayName("Read all teamLeads")
    public void readAllTest() throws SQLException {
        List<TeamLead> teamLeads = teamLeadService.readAll();
        Assertions.assertTrue(teamLeads.size() > 0);
    }

    @Test
    @DisplayName("Read teamLead by id")
    public void readTest() throws NotFoundException, SQLException {
        int testId = 1;
        TeamLead expected = new TeamLead(testId, "Test_name", Timestamp.valueOf("2020-01-01 12:10:10"));
        TeamLead actual = teamLeadService.read(testId);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Create and delete new teamLead")
    public void createTest() throws SQLException, AlreadyExistException, NotFoundException {
        int id = 222222;
        //create test project
        Project projectToInsert = new Project(id, "Test create", 123, "Test login", "Test password");
        projectService.create(projectToInsert);
        Project projectActual = projectService.read(id);
        Assertions.assertEquals(projectToInsert, projectActual);

        //create test teamLead
        TeamLead teamLeadToInsert = new TeamLead(id, "Test_name", new Timestamp(System.currentTimeMillis()));
        teamLeadService.create(teamLeadToInsert);
        TeamLead teamLeadActual = teamLeadService.read(id);
        Assertions.assertEquals(teamLeadToInsert.getId(), teamLeadActual.getId());
        Assertions.assertEquals(teamLeadToInsert.getDateOfBirth().getDate(), teamLeadActual.getDateOfBirth().getDate());

        //delete test teamLead
        teamLeadService.delete(id);
        Assertions.assertThrows(NotFoundException.class, () -> teamLeadService.read(id));

        //delete test project
        projectService.delete(id);
        Assertions.assertThrows(NotFoundException.class, () -> projectService.read(id));
    }
}
