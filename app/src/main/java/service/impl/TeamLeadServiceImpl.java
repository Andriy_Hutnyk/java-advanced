package service.impl;

import dao.TeamLeadDao;
import dao.impl.TeamLeadDaoImpl;
import exception.AlreadyExistException;
import exception.NotFoundException;
import model.TeamLead;
import org.apache.log4j.Logger;
import service.TeamLeadService;

import java.sql.SQLException;
import java.util.List;

public class TeamLeadServiceImpl implements TeamLeadService {


    private TeamLeadDao teamLeadDao;
    private static Logger logger = Logger.getLogger(TeamLeadServiceImpl.class);

    public TeamLeadServiceImpl() {
        teamLeadDao = new TeamLeadDaoImpl();
    }

    @Override
    public List<TeamLead> readAll() throws SQLException {
        logger.info("Get all teamLead request");
        return teamLeadDao.readAll();
    }

    @Override
    public TeamLead read(int id) throws SQLException, NotFoundException {
        TeamLead teamLead = teamLeadDao.read(id);
        if (teamLead == null) {
            logger.error("TeamLead with id: " + id + " not found and can not be read");
            throw new NotFoundException("TeamLead with id: " + id + " not found");
        } else {
            logger.info("Getting teamLead with id: " + id);
            return teamLead;
        }

    }

    @Override
    public void create(TeamLead teamLead) throws SQLException, AlreadyExistException {
        if (!isExists(teamLead.getId())) {
            logger.info("Creating teamLead " + teamLead);
            teamLeadDao.create(teamLead);
        } else {
            logger.error("TeamLead with id: " + teamLead.getId() + " already exist and can not be created");
            throw new AlreadyExistException("TeamLead with id: " + teamLead.getId() + " already exist");
        }
    }

    @Override
    public void delete(int id) throws SQLException, NotFoundException {
        if (isExists(id)) {
            logger.info("TeamLead with id: " + id + " was deleted");
            teamLeadDao.delete(id);
        } else {
            logger.error("TeamLead with id: " + id + " not found and can not be deleted");
            throw new NotFoundException("TeamLead with id: " + id + " not found");
        }
    }

    private List<TeamLead> getAll() throws SQLException {
        return teamLeadDao.readAll();
    }

    private boolean isExists(int id) throws SQLException {
        boolean flag = false;
        for (TeamLead teamLead : getAll()) {
            if (teamLead.getId() == id) {
                flag = true;
                break;
            }
        }
        return flag;
    }
}
