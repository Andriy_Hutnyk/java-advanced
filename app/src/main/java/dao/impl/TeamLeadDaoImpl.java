package dao.impl;

import dao.TeamLeadDao;
import model.TeamLead;
import util.MySqlConnector;
import util.SQLConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeamLeadDaoImpl implements TeamLeadDao {

    private Connection connection;

    public TeamLeadDaoImpl() {
        connection = MySqlConnector.getConnection();
    }

    @Override
    public List<TeamLead> readAll() throws SQLException {
        List<TeamLead> teamLeads = new ArrayList<>();
        try (Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQLConstants.GET_ALL_TEAM_LEAD)) {
            while (resultSet.next()) {
                TeamLead teamLead = new TeamLead(resultSet.getInt("id"),
                        resultSet.getString("full_name"), resultSet.getTimestamp("date_of_birth"));
                teamLeads.add(teamLead);
            }
        }
        return teamLeads;
    }

    @Override
    public TeamLead read(int id) throws SQLException {
        TeamLead teamLead = null;
        ResultSet resultSet = null;
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.GET_TEAM_LEAD_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                teamLead = new TeamLead(resultSet.getInt("id"),
                        resultSet.getString("full_name"), resultSet.getTimestamp("date_of_birth"));
            }
        }finally {
            resultSet.close();
        }
        return teamLead;
    }

    @Override
    public void create(TeamLead teamLead) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.INSERT_TEAM_LEAD)) {
            statement.setInt(1, teamLead.getId());
            statement.setString(2, teamLead.getFullName());
            statement.setTimestamp(3, teamLead.getDateOfBirth());
            statement.execute();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.DELETE_TEAM_LEAD_BY_ID)) {
            statement.setInt(1, id);
            statement.execute();
        }
    }
}
