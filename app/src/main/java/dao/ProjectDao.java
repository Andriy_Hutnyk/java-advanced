package dao;

import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Project;
import shared.AbstractCrudOperations;

import java.sql.SQLException;

public interface ProjectDao extends AbstractCrudOperations<Project> {

    void update(int id, Project current) throws SQLException, NotFoundException, AlreadyExistException;
}
