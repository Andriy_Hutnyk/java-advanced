package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import util.RandomIdGenerator;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeamLead {

    private int id;
    private String fullName;
    private Timestamp dateOfBirth;

    public TeamLead(String fullName, Timestamp dateOfBirth) {
        this.id = RandomIdGenerator.getRandomId();
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
    }
}
