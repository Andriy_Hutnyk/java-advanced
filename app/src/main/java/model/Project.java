package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import util.RandomIdGenerator;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Project {

    private int id;
    private String name;
    private double budget;
    private String login;
    private String password;

    public Project(String name, double budget, String login, String password) {
        this.id = RandomIdGenerator.getRandomId();
        this.name = name;
        this.budget = budget;
        this.login = login;
        this.password = password;
    }

}
