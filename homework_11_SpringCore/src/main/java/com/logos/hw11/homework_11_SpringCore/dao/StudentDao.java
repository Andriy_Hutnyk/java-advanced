package com.logos.hw11.homework_11_SpringCore.dao;

import com.logos.hw11.homework_11_SpringCore.model.Student;

public interface StudentDao extends AbstractCrudOperations<Student> {
}
