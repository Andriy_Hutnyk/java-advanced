package com.logos.hw11.homework_11_SpringCore.dao.impl;

import com.logos.hw11.homework_11_SpringCore.dao.StudentDao;
import com.logos.hw11.homework_11_SpringCore.model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StudentDaoImpl implements StudentDao {

    private List<Student> studentsTable;

    public StudentDaoImpl() {
        studentsTable = new ArrayList<>();
    }

    @Override
    public List<Student> readAll() {
        return studentsTable;
    }

    @Override
    public Student read(int id) {
        return studentsTable.stream().filter(e -> e.getId() == id).findFirst().orElseThrow(NullPointerException::new);
    }

    @Override
    public void create(Student student) {
        if (!studentsTable.contains(student)) studentsTable.add(student);
        else System.out.println("The student is already added");
    }

    @Override
    public void update(Student student, String property, String newValue) {
        int index = studentsTable.indexOf(student);
        Student updated = student;
        if (studentsTable.contains(student) && Objects.nonNull(property)) {
            switch (property) {
                case "name" : {
                    updated.setName(newValue);
                    break;
                }
                case  "age" : {
                    updated.setAge(Integer.parseInt(newValue));
                    break;
                }
                default: {
                    System.out.println("no such property to update");
                }
            }
            studentsTable.set(index, updated);
        }else {
            System.out.println("There is no student or property is null");
        }
    }

    @Override
    public void delete(int id) {
        Student studentToDelete = read(id);
        if (Objects.nonNull(studentToDelete)) studentsTable.remove(studentToDelete);
        else System.out.println("No student with id id : " + id);
    }
}
