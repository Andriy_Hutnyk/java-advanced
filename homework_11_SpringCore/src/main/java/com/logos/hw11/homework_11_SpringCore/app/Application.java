package com.logos.hw11.homework_11_SpringCore.app;

import com.logos.hw11.homework_11_SpringCore.dao.StudentDao;
import com.logos.hw11.homework_11_SpringCore.dao.impl.StudentDaoImpl;
import com.logos.hw11.homework_11_SpringCore.model.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);

		StudentDaoImpl service = (StudentDaoImpl) context.getBean("studentDao");
		Student student1 = (Student) context.getBean("student1");
		Student student2 = (Student) context.getBean("student2");
		Student student3 = (Student) context.getBean("student3");

		service.create(student1);
		service.create(student2);
		service.create(student3);

		System.out.println("Read all students");
		service.readAll().forEach(System.out::println);

		System.out.println("Read student by id");
		System.out.println(service.read(2));

		service.update(student3, "age", "55");
		System.out.println("Read all students after update");
		service.readAll().forEach(System.out::println);

		service.delete(3);
		System.out.println("Read all students after delete");
		service.readAll().forEach(System.out::println);

	}

}
