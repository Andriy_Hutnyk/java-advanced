package com.logos.hw11.homework_11_SpringCore.app;

import com.logos.hw11.homework_11_SpringCore.dao.StudentDao;
import com.logos.hw11.homework_11_SpringCore.dao.impl.StudentDaoImpl;
import com.logos.hw11.homework_11_SpringCore.model.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class StudentConfiguration {

    @Bean(name = "studentDao")
    @Scope("singleton")
    public StudentDao getShopDao() {
        return new StudentDaoImpl();
    }

    @Bean(name = "student1")
    public Student student1() {
        return new Student(1, "Andriy", 25);
    }

    @Bean(name = "student2")
    public Student student2() {
        return new Student(2, "Peter", 17);
    }

    @Bean(name = "student3")
    public Student student3() {
        return new Student(3, "Olga", 21);
    }
}
