import model.Comment;
import model.Post;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.SessionFactoryUtil;

import java.util.*;

public class Application {

    public static void main(String[] args) {
        Session session = SessionFactoryUtil.getSession();

        Transaction transaction = session.beginTransaction();

        Post post = new Post("Food");
        Comment comment1 = new Comment("Name-1", post);
        Comment comment2 = new Comment("Name-2", post);
        Comment comment3 = new Comment("Name-3", post);

        List<Comment> comments = new ArrayList<>(Arrays.asList(comment1, comment2, comment3));
        post.setComments(comments);

        session.save(post);

        Post postToRead = session.get(Post.class, 1);
        System.out.println(postToRead);

        Comment commentToRead = session.get(Comment.class, 2);
        System.out.println(commentToRead);

        transaction.commit();
        session.close();
    }
}
