package service;

import model.Project;
import util.AbstractCrudOperations;

public interface ProjectService extends AbstractCrudOperations<Project> {

    Project getProjectByLogin(String login);
}
