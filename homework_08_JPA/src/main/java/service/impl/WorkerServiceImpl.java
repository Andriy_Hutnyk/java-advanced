package service.impl;

import model.TeamLead;
import model.Worker;
import service.TeamLeadService;
import service.WorkerService;
import util.FactoryManager;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class WorkerServiceImpl implements WorkerService {

    private EntityManager entityManager = FactoryManager.getEntityManager();
    private TeamLeadService teamLeadService = new TeamLeadServiceImpl();

    @SuppressWarnings("unchecked")
    @Override
    public List<Worker> readAll() {
        Query query = entityManager.createQuery("SELECT w FROM Worker w");
        return (List<Worker>) query.getResultList();
    }

    @Override
    public Worker readByIds(int workerId, int teamLeadId) {
        Worker worker = new Worker(workerId, teamLeadService.read(teamLeadId));
        return entityManager.find(Worker.class, worker);
    }

    @Override
    public void create(Worker worker) {
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.persist(worker);
        entityManager.getTransaction().commit();
    }

    @Override
    public void delete(int workerId, int teamLeadId) {
        Worker worker = readByIds(workerId, teamLeadId);
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.remove(worker);
        entityManager.getTransaction().commit();
    }

}
