package service.impl;

import model.TeamLead;
import service.TeamLeadService;
import util.FactoryManager;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class TeamLeadServiceImpl implements TeamLeadService {

    private EntityManager entityManager = FactoryManager.getEntityManager();

    @Override
    public List<TeamLead> readAll() {
        Query query = entityManager.createQuery("SELECT t FROM TeamLead t");
        return (List<TeamLead>) query.getResultList();
    }

    @Override
    public TeamLead read(int id) {
        return entityManager.find(TeamLead.class, id);
    }

    @Override
    public void create(TeamLead teamLead) {
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.persist(teamLead);
        entityManager.getTransaction().commit();
    }

    @Override
    public void delete(int id) {
        TeamLead teamLead = read(id);
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.remove(teamLead);
        entityManager.getTransaction().commit();
    }
}
