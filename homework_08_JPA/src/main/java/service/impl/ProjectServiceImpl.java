package service.impl;

import model.Project;
import service.ProjectService;
import util.FactoryManager;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class ProjectServiceImpl implements ProjectService {

    private EntityManager entityManager = FactoryManager.getEntityManager();

    @Override
    public Project getProjectByLogin(String login) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Project> criteria = builder.createQuery(Project.class);
        Root<Project> from = criteria.from(Project.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("login"), login));
        return entityManager.createQuery(criteria).getSingleResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Project> readAll() {
        Query query = entityManager.createQuery("SELECT p FROM Project p");
        return (List<Project>) query.getResultList();
    }

    @Override
    public Project read(int id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public void create(Project project) {
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.persist(project);
        entityManager.getTransaction().commit();
    }

    @Override
    public void delete(int id) {
        Project project = read(id);
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        entityManager.remove(project);
        entityManager.getTransaction().commit();
    }
}
