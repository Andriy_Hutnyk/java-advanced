package service;

import model.TeamLead;
import util.AbstractCrudOperations;

public interface TeamLeadService extends AbstractCrudOperations<TeamLead> {

}
