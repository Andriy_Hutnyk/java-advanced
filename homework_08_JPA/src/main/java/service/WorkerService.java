package service;

import model.Worker;

import java.util.List;

public interface WorkerService {

    public List<Worker> readAll();

    Worker readByIds(int workerId, int teamLeadId);

    public void create(Worker worker);

    public void delete(int workerId, int teamLeadId);
}
