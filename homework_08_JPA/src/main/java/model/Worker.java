package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "worker")
public class Worker implements Serializable {

    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "full_name")
    private String fullName;
    @Column(name = "title")
    private String title;
    @Column(name = "salary")
    private double salary;

    @Id
    @ManyToOne
    @JoinColumn(name = "team_lead_id", referencedColumnName = "id")
    private TeamLead teamLead;


    public Worker(int id, TeamLead teamLead) {
        this.id = id;
        this.teamLead = teamLead;
    }
}
