import model.Project;
import model.TeamLead;
import model.Worker;
import service.ProjectService;
import service.TeamLeadService;
import service.WorkerService;
import service.impl.ProjectServiceImpl;
import service.impl.TeamLeadServiceImpl;
import service.impl.WorkerServiceImpl;

import java.sql.Timestamp;

public class Main {

    private static ProjectService projectService = new ProjectServiceImpl();
    private static WorkerService workerService = new WorkerServiceImpl();
    private static TeamLeadService teamLeadService = new TeamLeadServiceImpl();

    public static void main(String[] args) {
        //projectService.readAll().forEach(System.out::println);
        //System.out.println(projectService.read(1));
        //projectService.create(new Project(5, "kvcmsl", 12, "qwertyu", "aqwedfgh"));
        //projectService.delete(5);
        //System.out.println(projectService.getProjectByLogin("123123"));


        //workerService.readAll().forEach(System.out::println);
        //System.out.println(workerService.readByIds(1, 1));
        //workerService.create(new Worker(2, "Andriy", "programmer", 10000, teamLeadService.read(1)));
        //workerService.delete(2, 1);


        //teamLeadService.readAll().forEach(System.out::println);
        //System.out.println(teamLeadService.read(1));
        //teamLeadService.create(new TeamLead(2, "Test team lead", new Timestamp(System.currentTimeMillis())));
        //teamLeadService.delete(2);

    }
}
