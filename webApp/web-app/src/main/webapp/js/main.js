$('.message a').click(function () {
    $('form').animate({
        height: "toggle",
        opacity: "toggle"
    }, "slow");
});


$(document).ready(function () {
    $("button#register-button").click(function () {
        var projectName = $("form.register-form input.projectName").val();
        var login = $("form.register-form input.login").val();
        var password = $("form.register-form input.password").val();
        var confirmPassword = $("form.register-form input.confirmPassword").val();
        if (projectName == '' || login == '' || password == '' || confirmPassword == '') {
            alert("Please fill all fields...!!!!!!");
        } else if ((password.length) < 8) {
            alert("Password should at least have 8 character...!!!!!!");
        } else if (!(password).match(confirmPassword)) {
            alert("Your passwords don't match. Try again?");
        } else {
            var projectRegistration = {
                projectName: projectName,
                login: login,
                password: password
            };
            $.post("registration", projectRegistration, function (data) {
                if (data == 'Success') {
                    $("form")[0].reset();
                }
            });
        }
    });
});


$(document).ready(function () {
    $("button#login-button").click(function () {
        var login = $("form.login-form input.login").val();
        var password = $("form.login-form input.password").val();

        if (login == '' || password == '') {
            alert("Please fill all fields...!!!!!!");
        } else {
            var project = {
                login: login,
                password: password
            };
            $.post("login", project, function (data) {
                redirection(data);
                $("form")[1].reset();
            });
        }
    });
});

function redirection(data) {
    if (data != '') {
        let finalUrl = '';
        let url = window.location.href.split('/');
        for (let i = 0; i < url.length - 1; i++) {
            finalUrl += url[i] + '/';
        }
        finalUrl += data.destinationUrl;
        window.location.href = finalUrl;
    }
}
