package service.impl;

import dao.WorkerDao;
import dao.impl.WorkerDaoImpl;
import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Worker;
import org.apache.log4j.Logger;
import service.WorkerService;

import java.sql.SQLException;
import java.util.List;

public class WorkerServiceImpl implements WorkerService {


    private WorkerDao workerDao;
    private static Logger logger = Logger.getLogger(WorkerServiceImpl.class);

    public WorkerServiceImpl() {
        workerDao = new WorkerDaoImpl();
    }

    @Override
    public List<Worker> readAll() throws SQLException {
        return workerDao.readAll();
    }

    @Override
    public Worker read(int id) throws SQLException, NotFoundException {
        Worker worker = workerDao.read(id);
        if (worker == null) {
            logger.error("Worker with id: " + id + " not found and can not be read");
            throw new NotFoundException("Worker with id: " + id + " not found");
        } else {
            logger.info("Getting worker with id: " + id);
            return worker;
        }
    }

    @Override
    public void create(Worker worker) throws SQLException, AlreadyExistException {
        if (!isExists(worker.getId())) {
            logger.info("Creating worker " + worker);
            workerDao.create(worker);
        } else {
            logger.error("Worker with id: " + worker.getId() + " already exist and can not be created");
            throw new AlreadyExistException("Worker with id: " + worker.getId() + " already exist");
        }
    }

    @Override
    public void update(int id, Worker current) throws SQLException, AlreadyExistException, NotFoundException {
        if (isExists(id)) {
            if (!isExists(current.getId())) {
                logger.info("Worker with id " + id + " was updated by worker " + current);
                workerDao.update(id, current);
            } else {
                logger.error("Worker with id: " + current.getId() + " already exist. Chose another id to update");
                throw new AlreadyExistException("Worker with id: " + current.getId() + " already exist");
            }
        } else {
            logger.error("Worker with id: " + id + " not found and can not be updated");
            throw new NotFoundException("Worker with id: " + id + " not found");
        }
    }

    @Override
    public void delete(int id) throws SQLException, NotFoundException {
        if (isExists(id)) {
            logger.info("Worker with id: " + id + " was deleted");
            workerDao.delete(id);
        } else {
            logger.error("Worker with id: " + id + " not found and can not be deleted");
            throw new NotFoundException("Worker with id: " + id + " not found");
        }
    }

    private List<Worker> getAll() throws SQLException {
        return workerDao.readAll();
    }

    private boolean isExists(int id) throws SQLException {
        boolean flag = false;
        for (Worker worker : getAll()) {
            if (worker.getId() == id) {
                flag = true;
                break;
            }
        }
        return flag;
    }
}
