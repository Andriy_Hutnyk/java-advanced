package service.impl;

import dao.impl.ProjectDaoImpl;
import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Project;
import org.apache.log4j.Logger;
import service.ProjectService;

import java.sql.SQLException;
import java.util.List;

public class ProjectServiceImpl implements ProjectService {

    private ProjectDaoImpl projectDao;
    private static final Logger logger = Logger.getLogger(ProjectServiceImpl.class);

    public ProjectServiceImpl() {
        projectDao = new ProjectDaoImpl();
    }

    @Override
    public List<Project> readAll() throws SQLException {
        logger.info("Get all projects request");
        return projectDao.readAll();
    }

    @Override
    public Project read(int id) throws SQLException, NotFoundException {
        Project project = projectDao.read(id);
        if (project == null) {
            logger.error("Project with id: " + id + " not found and can not be read");
            throw new NotFoundException("Project with id: " + id + " not found");
        } else {
            logger.info("Getting project with id: " + id);
            return project;
        }
    }

    @Override
    public Project readByLogin(String login) throws SQLException {
        return projectDao.readByLogin(login);
    }

    @Override
    public void create(Project project) throws SQLException, AlreadyExistException {
        if (!isExists(project.getId())) {
            logger.info("Creating project " + project);
            projectDao.create(project);
        } else {
            logger.error("Project with id: " + project.getId() + " already exist and can not be created");
            throw new AlreadyExistException("Project with id: " + project.getId() + " already exist");
        }
    }

    @Override
    public void update(int id, Project current) throws SQLException, NotFoundException, AlreadyExistException {
        if (!isExists(id)) {
            logger.error("Project with id: " + id + " not found and can not be updated");
            throw new NotFoundException("Project with id: " + id + " not found");
        } else {
            logger.info("Project with id " + id + " was updated by project " + current);
            projectDao.update(id, current);
        }
    }

    @Override
    public void delete(int id) throws SQLException, NotFoundException {
        if (isExists(id)) {
            logger.info("Project with id: " + id + " was deleted");
            projectDao.delete(id);
        } else {
            logger.error("Project with id: " + id + " not found and can not be deleted");
            throw new NotFoundException("Project with id: " + id + " not found");
        }
    }

    private List<Project> getAll() throws SQLException {
        return projectDao.readAll();
    }

    private boolean isExists(int projectId) throws SQLException {
        boolean flag = false;
        for (Project project : getAll()) {
            if (project.getId() == projectId) {
                flag = true;
                break;
            }
        }
        return flag;
    }
}
