package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import util.RandomIdGenerator;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Worker {

    private int id;
    private String fullName;
    private String title;
    private double salary;
    private int teamLeadId;

    public Worker(String fullName, String title, double salary) {
        this.id = RandomIdGenerator.getRandomId();
        this.fullName = fullName;
        this.title = title;
        this.salary = salary;
    }
}
