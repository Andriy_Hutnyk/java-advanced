package dao.impl;

import dao.ProjectDao;
import model.Project;
import util.MySqlConnector;
import util.SQLConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectDaoImpl implements ProjectDao {

    private Connection connection;

    public ProjectDaoImpl() {
        connection = MySqlConnector.getConnection();
    }

    @Override
    public List<Project> readAll() throws SQLException {
        List<Project> projects = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQLConstants.GET_ALL_PROJECTS)) {
            while (resultSet.next()) {
                Project project = new Project(resultSet.getInt("id"), resultSet.getString("name"),
                        resultSet.getDouble("budget"), resultSet.getString("login"),
                        resultSet.getString("password"));
                projects.add(project);
            }
        }
        return projects;
    }

    @Override
    public Project read(int id) throws SQLException {
        ResultSet resultSet = null;
        Project project = null;
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.GET_PROJECT_BY_ID)) {
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                project = new Project(resultSet.getInt("id"), resultSet.getString("name"),
                        resultSet.getDouble("budget"), resultSet.getString("login"),
                        resultSet.getString("password"));
            }
        } finally {
            resultSet.close();
        }
        return project;
    }

    @Override
    public Project readByLogin(String login) throws SQLException {
        ResultSet resultSet = null;
        Project project = null;
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.GET_PROJECT_BY_LOGIN)) {
            statement.setString(1, login);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                project = new Project(resultSet.getInt("id"), resultSet.getString("name"),
                        resultSet.getDouble("budget"), resultSet.getString("login"),
                        resultSet.getString("password"));
            }
        } finally {
            resultSet.close();
        }
        return project;
    }

    @Override
    public void create(Project project) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.INSERT_PROJECT)) {
            statement.setInt(1, project.getId());
            statement.setString(2, project.getName());
            statement.setDouble(3, project.getBudget());
            statement.setString(4, project.getLogin());
            statement.setString(5, project.getPassword());
            statement.execute();
        }
    }

    @Override
    public void update(int id, Project current) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.UPDATE_PROJECT)) {
            statement.setInt(1, current.getId());
            statement.setString(2, current.getName());
            statement.setDouble(3, current.getBudget());
            statement.setString(4, current.getLogin());
            statement.setString(5, current.getPassword());
            statement.setInt(6, id);
            statement.execute();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(SQLConstants.DELETE_PROJECT_BY_ID)) {
            statement.setInt(1, id);
            statement.execute();
        }
    }
}
