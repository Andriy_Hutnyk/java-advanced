package util;

public class SQLConstants {

    public static final String GET_ALL_PROJECTS = "SELECT * FROM it_project.project";
    public static final String GET_PROJECT_BY_ID = "SELECT * FROM it_project.project WHERE id = ?";
    public static final String GET_PROJECT_BY_LOGIN = "SELECT * FROM it_project.project WHERE login = ?";
    public static final String INSERT_PROJECT = "INSERT INTO it_project.project (id, name, budget, login, password) VALUES (?, ?, ?, ?, ?)";
    public static final String UPDATE_PROJECT = "UPDATE it_project.project SET id = ?, name = ?, budget = ?, login = ?, password = ? WHERE (id = ?)";
    public static final String DELETE_PROJECT_BY_ID = "DELETE FROM it_project.project WHERE id = ?";

    public static final String GET_ALL_TEAM_LEAD = "SELECT * FROM it_project.team_lead";
    public static final String GET_TEAM_LEAD_BY_ID = "SELECT * FROM it_project.team_lead WHERE id = ?";
    public static final String INSERT_TEAM_LEAD = "INSERT INTO it_project.team_lead (id, full_name, date_of_birth) VALUES (?, ?, ?)";
    //public static final String UPDATE_TEAM_LEAD = "UPDATE it_project.team_lead SET id = ?, full_name = ?, date_of_birth = ?  WHERE (id = ?)";
    public static final String DELETE_TEAM_LEAD_BY_ID = "DELETE FROM it_project.team_lead WHERE id = ?";

    public static final String GET_ALL_WORKERS = "SELECT * FROM it_project.worker";
    public static final String GET_WORKER_BY_ID = "SELECT * FROM it_project.worker WHERE id = ?";
    public static final String INSERT_WORKER = "INSERT INTO it_project.worker (id, full_name, title, salary) VALUES (?, ?, ?, ?)";
    public static final String UPDATE_WORKER = "UPDATE it_project.worker SET id = ?, full_name = ?, title = ?, salary = ? WHERE (id = ?)";
    public static final String DELETE_WORKER_BY_ID = "DELETE FROM it_project.worker WHERE id = ?";
}
