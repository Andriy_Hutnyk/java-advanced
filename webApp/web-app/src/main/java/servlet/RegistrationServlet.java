package servlet;

import lombok.SneakyThrows;
import model.Project;
import org.apache.log4j.Logger;
import service.ProjectService;
import service.impl.ProjectServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;


@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {

    private ProjectService projectService;
    private static final Logger logger = Logger.getLogger(RegistrationServlet.class);

    public RegistrationServlet() {
        projectService = new ProjectServiceImpl();
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String login = req.getParameter("login");

        if (Objects.isNull(projectService.readByLogin(login))) {
            logger.info("Registration for new Project");
            String name = req.getParameter("projectName");
            String password = req.getParameter("password");
            if (!name.isEmpty() && !login.isEmpty() && !password.isEmpty()) {
                Project project = new Project(name, login, password);
                projectService.create(project);
                logger.info("Project was registered " + project);

                HttpSession session = req.getSession(true);
                session.setAttribute("projectName", name);
                session.setAttribute("projectLogin", login);
            }
        } else {
            logger.info("Project with login: " + login + " already registered. Redirection to login page");
            //TODO : redirect project to login page if login is already in use
        }

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write("Success");
    }
}
