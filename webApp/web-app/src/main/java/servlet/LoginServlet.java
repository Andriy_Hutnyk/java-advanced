package servlet;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import model.Project;
import org.apache.log4j.Logger;
import service.ProjectService;
import service.impl.ProjectServiceImpl;
import util.ProjectLogin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private ProjectService projectService;
    private static final Logger logger = Logger.getLogger(LoginServlet.class);

    public LoginServlet() {
        projectService = new ProjectServiceImpl();
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");

        Project project = projectService.readByLogin(login);
        if (!Objects.isNull(project)) {
            String password = req.getParameter("password");
            if (project.getPassword().equals(password)) {
                logger.info("Project with login: " + login + " was logged in");
                ProjectLogin projectLogin = new ProjectLogin(login, "cabinet.jsp");

                HttpSession session = req.getSession(true);
                session.setAttribute("projectName", project.getName());
                session.setAttribute("projectLogin", login);

                String json = new Gson().toJson(projectLogin);
                logger.info("JSON login " + json);
                resp.setContentType("application/json");
                resp.setCharacterEncoding("UTF-8");
                resp.getWriter().write(json);
            } else {
                logger.info("Wrong password for project with login: " + login);
                //TODO : create redirection to login.jsp
            }
        } else {
            logger.info("Project with login " + login + " is not registered. Redirection to registration page");
            //TODO : redirect to registration page
        }
    }
}
