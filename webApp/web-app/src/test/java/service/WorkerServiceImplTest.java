package service;

import exception.NotFoundException;
import model.Worker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import service.impl.ProjectServiceImpl;
import service.impl.TeamLeadServiceImpl;
import service.impl.WorkerServiceImpl;

import java.sql.SQLException;
import java.util.List;

public class WorkerServiceImplTest {

    private static WorkerServiceImpl workerService;
    private static TeamLeadServiceImpl teamLeadService;
    private static ProjectServiceImpl projectService;

    @BeforeAll
    static void init() {
        workerService = new WorkerServiceImpl();
        teamLeadService = new TeamLeadServiceImpl();
        projectService = new ProjectServiceImpl();
    }

    @Test
    @DisplayName("Read all workers")
    public void readAllTest() throws SQLException {
        List<Worker> workers = workerService.readAll();
        Assertions.assertTrue(workers.size() > 0);
    }

    @Test
    @DisplayName("Read a worker")
    public void readTest () throws NotFoundException, SQLException {
        int id = 1;
        Worker expected = new Worker(id, "Test_name", "test_title", 100.00, 1);
        Worker actual = workerService.read(id);
        Assertions.assertEquals(expected, actual);
    }

}
