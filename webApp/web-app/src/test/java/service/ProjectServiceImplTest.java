package service;

import exception.AlreadyExistException;
import exception.NotFoundException;
import model.Project;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import service.impl.ProjectServiceImpl;

import java.sql.SQLException;
import java.util.List;

public class ProjectServiceImplTest {

    private static ProjectServiceImpl projectService;

    @BeforeAll
    static void init() {
        projectService = new ProjectServiceImpl();
    }

    @Test
    @DisplayName("Read all projects")
    public void readAllTest() throws SQLException {
        List<Project> projectList = projectService.readAll();
        Assertions.assertTrue(projectList.size() > 0);
    }

    @Test
    @DisplayName("Read project by id")
    public void readTest() throws NotFoundException, SQLException {
        int testId = 1;
        Project expected = new Project(testId, "AAA", 123, "123123", "123123");
        Project actual = projectService.read(testId);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Create and delete new project")
    public void createTest() throws SQLException, AlreadyExistException, NotFoundException {
        int id = 2222;
        Project projectToInsert = new Project(id, "Test create", 123, "Test login", "Test password");
        projectService.create(projectToInsert);
        Project actual = projectService.read(id);
        Assertions.assertEquals(projectToInsert, actual);

        projectService.delete(id);
        Assertions.assertThrows(NotFoundException.class, () -> projectService.read(id));

    }

    @Test
    @DisplayName("Update project")
    public void updateTest() throws NotFoundException, SQLException, AlreadyExistException {
        int id = 1;
        Project previous = projectService.read(id);
        Project current = new Project(id, "Test update", 321, "login", "password");
        projectService.update(id, current);
        Assertions.assertEquals(current, projectService.read(id));

        projectService.update(id, previous);
        Assertions.assertEquals(previous, projectService.read(id));
    }

}
